-- MySQL dump 10.13  Distrib 5.5.56, for Linux (x86_64)
--
-- Host: localhost    Database: cigo_admin_demo
-- ------------------------------------------------------
-- Server version	5.5.56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cg_edit_demo`
--

DROP TABLE IF EXISTS `cg_edit_demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_edit_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(512) CHARACTER SET utf8mb4 NOT NULL COMMENT '标题',
  `radio_landscape` varchar(512) NOT NULL DEFAULT '' COMMENT 'Radio横向',
  `radio_portrait` varchar(512) NOT NULL DEFAULT '' COMMENT 'Radio纵向',
  `checkbox_landscape` varchar(512) NOT NULL DEFAULT '' COMMENT 'CheckBox横向',
  `checkbox_portrait` varchar(512) NOT NULL DEFAULT '' COMMENT 'CheckBox纵向',
  `label-class` varchar(512) NOT NULL COMMENT '标签样式',
  `img` varchar(1024) DEFAULT '' COMMENT '单图',
  `img-multi` varchar(1024) DEFAULT '' COMMENT '多图',
  `img-show` varchar(1024) DEFAULT '' COMMENT '图片橱窗',
  `build-date` int(11) DEFAULT '0' COMMENT '创建日期',
  `summary` varchar(1024) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '简介',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='编辑测试表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cg_edit_demo`
--

LOCK TABLES `cg_edit_demo` WRITE;
/*!40000 ALTER TABLE `cg_edit_demo` DISABLE KEYS */;
INSERT INTO `cg_edit_demo` VALUES (1,'asdfasdf','0','0','','','label-default','','','',1528214400,'',0,51,1528261800),(2,'asdfasdfasdf','0','0','','','label-default','','','',1528506366,'',0,50,1528506366),(3,'asdfasdfasdf','0','0','','','label-default','','','',1528506372,'',0,50,1528506372),(4,'dfhdfhdfhd','0','0','','','label-default','','','',1528506378,'',0,50,1528506378);
/*!40000 ALTER TABLE `cg_edit_demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cg_files`
--

DROP TABLE IF EXISTS `cg_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '文件类型',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '文件名',
  `ext` varchar(32) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` varchar(64) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `path` varchar(1024) NOT NULL DEFAULT '' COMMENT '文件路径名',
  `path_cover` varchar(1024) NOT NULL DEFAULT '' COMMENT '文件封面图片路径字符串',
  `thumb_small` varchar(1024) NOT NULL DEFAULT '' COMMENT '图片缩略图-小',
  `thumb_middle` varchar(1024) NOT NULL DEFAULT '' COMMENT '图片缩略图-中',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='上传文件表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cg_files`
--

LOCK TABLES `cg_files` WRITE;
/*!40000 ALTER TABLE `cg_files` DISABLE KEYS */;
INSERT INTO `cg_files` VALUES (1,1,'1528521772LtLtJ','jpg','image/jpeg','./Upload/images/admin/2018-06-09/1528521772LtLtJ.jpg','','./Upload/images/admin/2018-06-09/1528521772LtLtJ_thumb_small.jpg','./Upload/images/admin/2018-06-09/1528521772LtLtJ_thumb_middle.jpg','d909484403794e75422177159ce0537c','4e28e675da985e010677bdb071d9647a9a4b1814',0,0),(2,1,'1528527235qH6g7','jpg','image/jpeg','./Upload/images/admin/2018-06-09/1528527235qH6g7.jpg','','','','c5238b7601f595b3caa92c33bc40ddbf','3483119158e64bb2a3e000a23341963c50863b35',0,0);
/*!40000 ALTER TABLE `cg_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cg_manager`
--

DROP TABLE IF EXISTS `cg_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(256) NOT NULL COMMENT '密码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `last_log_ip` int(11) unsigned NOT NULL COMMENT '上次登陆IP',
  `last_log_time` int(11) unsigned NOT NULL COMMENT '上次登陆时间',
  `create_time` int(11) unsigned NOT NULL COMMENT '注册日期',
  `create_ip` int(11) unsigned NOT NULL COMMENT '注册IP',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台管理员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cg_manager`
--

LOCK TABLES `cg_manager` WRITE;
/*!40000 ALTER TABLE `cg_manager` DISABLE KEYS */;
INSERT INTO `cg_manager` VALUES (1,'manager','管理员','OGQ5NjllZWY2ZWNhZDNjMjlhM2E2MjkyODBlNjg2Y2YwYzNmNWQ1YTg2YWZmM2NhMTIwMjBjOTIzYWRjNmM5Mg==',1,3232278529,1528505578,1468636730,0,0);
/*!40000 ALTER TABLE `cg_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cg_menu_admin`
--

DROP TABLE IF EXISTS `cg_menu_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_menu_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pid` int(11) NOT NULL COMMENT '父级菜单编号',
  `path` varchar(2048) NOT NULL DEFAULT '0,' COMMENT '菜单路径Path',
  `group` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '数据分组',
  `group_sort` int(11) NOT NULL DEFAULT '0' COMMENT 'Group排序',
  `icon` varchar(250) DEFAULT '' COMMENT '图标',
  `title` varchar(250) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `label_class` varchar(250) NOT NULL COMMENT '标签Label样式Class',
  `url` varchar(1024) DEFAULT NULL COMMENT '菜单地址',
  `target` varchar(256) NOT NULL DEFAULT '_blank' COMMENT '链接target',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `show_top_menu` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在顶部菜单显示',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `opt_rate` int(11) NOT NULL DEFAULT '0' COMMENT '操作频率',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `menu_admin_path` (`path`(333))
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cg_menu_admin`
--

LOCK TABLES `cg_menu_admin` WRITE;
/*!40000 ALTER TABLE `cg_menu_admin` DISABLE KEYS */;
INSERT INTO `cg_menu_admin` VALUES (1,0,'0,','系统维护',80,'cigo-icon-menu','后台菜单','label-primary','AdminMenu/index','page_content',1,0,50,274),(33,0,'0,','',100,'cigo-icon-c11_show','数据统计','label-default','Center/index','page_content',-2,0,501,252),(34,0,'0,','商品订单管理',60,'cigo-icon-dingdanguanli','订单管理','label-default','','page_content',-1,1,50,197),(37,34,'0,34,','',50,'cigo-icon-dingdantongji','订单统计','label-default','OrderSum/index','page_content',0,1,50,13),(9,8,'0,4,7,8,','',50,'','Add Sub-SubMenu','label-default','','_blank',0,1,0,0),(36,34,'0,34,','',50,'cigo-icon-order-log','订单日志','label-default','OrderLog/index','page_content',1,1,50,12),(35,34,'0,34,','',50,'cigo-icon-dingdan','订单列表','label-default','Order/index','page_content',1,1,50,24),(32,0,'0,','',50,'','测试假删除','label-default','','page_content',-2,1,0,0),(11,0,'0,','后台管理',90,'cigo-icon-data','数据维护','label-default','','page_content',1,0,50,335),(12,11,'0,11,','数据库',50,'cigo-icon-backup','备份数据','label-default','Backup/index','page_content',1,1,50,13),(13,11,'0,11,','数据库',50,'cigo-icon-restore','恢复数据','label-default','Restore/index','page_content',1,1,50,10),(14,11,'0,11,','',50,'cigo-icon-huishouzhan','回收站','label-default','Trash/index','page_content',1,1,50,49),(17,16,'0,16,','',50,'cigo-icon-chanpinleimu','商品分类','label-default','GoodsCategory/index','page_content',1,1,60,128),(16,0,'0,','商品订单管理',60,'cigo-icon-goods','商品管理','label-default','','page_content',-1,1,50,2114),(18,16,'0,16,','',50,'cigo-icon-shangpinfenlei01','商品列表','label-default','Goods/index','page_content',-1,1,100,381),(19,16,'0,16,','',50,'cigo-icon-pinpai','品牌管理','label-default','GoodsBrand/index','page_content',0,1,58,30),(20,16,'0,16,','',50,'cigo-icon-shangpinshuxing','商品属性','label-default','GoodsProperty/index','page_content',0,1,50,0),(21,16,'0,16,','',50,'cigo-icon-xuanzeguigekongzhuangtai','规格管理','label-default','Spec/index','page_content',1,1,56,122),(22,16,'0,16,','',50,'cigo-icon-zixun','商品咨询','label-default','GoodsAsk/index','page_content',0,1,50,2),(23,16,'0,16,','',50,'cigo-icon-pinglun','商品评论','label-default','GoodsComment/index','page_content',1,1,62,53),(24,0,'0,','系统维护',80,'cigo-icon-permission','权限管理','label-default','','page_content',1,0,40,150),(25,24,'0,24,','',50,'cigo-icon-guanliyuan','管理员维护','label-default','Admin/index','page_content',1,1,50,9),(26,24,'0,24,','',50,'cigo-icon-jiaoseguanli','角色管理','label-default','Role/index','page_content',1,1,50,12),(27,24,'0,24,','',50,'cigo-icon-gongyingshang','供应商维护','label-default','Supplier/index','page_content',-2,1,50,6),(28,24,'0,24,','',50,'cigo-icon-rizhi','操作日志','label-default','LogAdmin/index','page_content',1,1,50,4),(29,0,'0,','网站维护',50,'cigo-icon-xitongcanshupeizhi','网站设置','label-default','','page_content',-2,0,50,38),(30,0,'0,','网站维护',48,'cigo-icon-youqinglianjie','友情链接','label-default','FriendLink/index','page_content',-1,0,50,2),(31,0,'0,','后台管理',90,'cigo-icon-peizhicanshu','网站设置','label-default','SystemConfigSetting/index','page_content',1,0,50,75),(38,0,'0,','城市区域管理',55,'cigo-icon-wlxxgl','物流配送管理','label-default','','page_content',-1,1,50,466),(39,38,'0,38,','',50,'cigo-icon-wuliuguanli','物流模版','label-default','ShipTpl/index','page_content',1,1,52,124),(40,38,'0,38,','',50,'cigo-icon-fahuo','发货管理','label-default','Delivery/index','page_content',1,1,50,19),(41,0,'0,','商品订单管理',60,'cigo-icon-danpincuxiaoguanli','促销管理','label-default','','page_content',-1,1,50,236),(42,16,'0,41,','',50,'cigo-icon-pintuanicon','拼团管理','label-default','GroupBuy/index','page_content',1,1,50,131),(43,41,'0,41,','',50,'cigo-icon-coupon','优惠券管理','label-default','Coupon/index','page_content',1,1,50,0),(44,38,'0,38,','',50,'cigo-icon-quyu','配送区域及费用管理','label-default','ShipAreaConfig/index','page_content',-2,1,51,4),(45,16,'0,16,','',50,'cigo-icon-shangpinfenlei','规格类型','label-default','SpecType/index','page_content',1,1,54,43),(46,0,'0,','',50,'','待删除数据测试','label-default','','page_content',-2,1,0,0),(47,0,'0,','自定义编辑插件',0,'cigo-icon-demozizhan','功能演示','label-default','EditDemo/index','page_content',1,0,50,121),(48,47,'0,47,','',50,'','Test','label-default','','page_content',-2,1,50,0),(49,48,'0,47,48,','',50,'','tast1','label-default','','page_content',-2,1,50,0),(51,52,'0,29,','',50,'cigo-icon-guanggaowei','广告位管理','label-default','AdvPosition/Index','page_content',1,1,50,14),(50,52,'0,31,','',50,'cigo-icon-leixing','广告打开类型','label-default','AdvOpenType/Index','page_content',1,0,50,23),(52,0,'0,','网站维护',48,'cigo-icon-guanggao','广告管理','label-default','','page_content',-1,1,50,87),(54,55,'0,55,','',0,'cigo-icon-wendang','App接口文档','label-default','AppApiDoc/index','page_content',1,1,50,161),(53,52,'0,52,','',50,'cigo-icon-liebiao','广告列表','label-default','Adv/Index','page_content',1,1,50,14),(58,0,'0,','城市区域管理',55,'cigo-icon-quyu','开放城市管理','label-default','CityOpen','page_content',-1,1,50,10),(55,0,'0,','系统维护',80,'cigo-icon-xitongcanshupeizhi','开发接口对接','label-default','','page_content',-2,0,41,1022),(56,0,'0,','系统维护',80,'cigo-icon-xitongcanshu','系统参数维护','label-default','SystemConfigManager/index','page_content',1,0,50,79),(57,55,'0,55,','',0,'cigo-icon-APIjiekou','App开发调试接口','label-default','AppApiDoc/api','page_content',1,1,50,163),(59,0,'0,','城市区域管理',55,'cigo-icon-remen','热门城市管理','label-default','CityHot/index','page_content',-1,0,50,20),(60,0,'0,','后台管理',90,'cigo-icon-chengshi','省市区域维护','label-default','Region/index','page_content',-2,0,50,12),(61,0,'0,','',100,'cigo-icon-bangzhu','App帮助','label-default','AppHelp/index','page_content',-1,0,40,42),(62,0,'0,','',100,'cigo-icon-bannerguanli','Banner管理','label-default','Banner/index','page_content',-1,1,41,140),(63,0,'0,','',100,'cigo-icon-lingyu','领域维护','label-default','Field/index','page_content',-1,1,50,392),(64,0,'0,','',100,'cigo-icon-guanjianci','关键词维护','label-default','Keywords/index','page_content',-1,1,50,567),(65,0,'0,','',100,'cigo-icon-tieziguanli','帖子管理','label-default','Note/index','page_content',-1,1,50,509),(66,0,'0,','',100,'cigo-icon-pinglunguanli','评论管理','label-default','Comment/index','page_content',-1,1,50,59),(67,0,'0,','',100,'cigo-icon-gukeguanli','用户管理','label-default','UserManager/index','page_content',-1,1,50,336),(68,0,'0,','后台管理',90,'cigo-icon-shangchuanwenjian','上传文件管理','label-default','Files/index','page_content',1,1,50,25),(69,0,'0,','',100,'cigo-icon-zhifeiji','用户反馈','label-default','UserFeedback/index/status/0','page_content',-1,0,51,227),(70,0,'0,','',100,'cigo-icon-tousu','用户投诉','label-default','UserAccusation/index','page_content',-1,1,40,57),(71,0,'0,','',100,'cigo-icon-zhuanti','专题活动','label-default','SpecialEvent/index','page_content',-2,1,50,135),(72,55,'0,55,','',100,'cigo-icon-APIjiekou','住哲开发接口对接','label-default','Zhuzhe/index','page_content',1,1,50,0),(73,0,'0,','',100,'','基础数据管理','label-default','','page_content',-2,1,50,19),(74,73,'0,73,','',100,'','开放城市管理','label-default','OpenCity/index','page_content',1,1,50,4);
/*!40000 ALTER TABLE `cg_menu_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cg_trash`
--

DROP TABLE IF EXISTS `cg_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cg_trash` (
  `data_id` int(11) NOT NULL COMMENT '数据编号',
  `type` int(11) NOT NULL COMMENT '数据类型',
  `title` varchar(250) CHARACTER SET utf8mb4 NOT NULL COMMENT '标题',
  `create_time` int(11) NOT NULL COMMENT '删除日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='回收站表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cg_trash`
--

LOCK TABLES `cg_trash` WRITE;
/*!40000 ALTER TABLE `cg_trash` DISABLE KEYS */;
INSERT INTO `cg_trash` VALUES (62,1,'Banner管理',0),(67,1,'用户管理',0),(69,1,'用户反馈',0);
/*!40000 ALTER TABLE `cg_trash` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-09 15:01:53
